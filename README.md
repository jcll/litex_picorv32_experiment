# Simple experiments around RISC-V

## Introduction
This repository contains my own experiment on using a RISC-V softcore on a FPGA, using a particular approach : everything here is described using a Python DSL named Migen.

### Requirements

- Litex install. It contains migen itself.
- riscv cross compilation toolchain.
- serial terminal like Gtkterm or minicom.
- be root on your Linux system.

I suggest reading Litex webpage.

## Build a picorv32-based SoC  on Nexys4ddr:

 Build process : 4 ordered steps, as 1..4. Run them as written :
  - ./1_build_picorv32_nexys4ddr.py
  - ./2_configure_fpga.x
  - ./3_build_firmware.x
  - ./4_play.x

Trouble shooting : depending on your system, you may have trouble running the 4th step.
  Pyserial may emit exception regarding rights on /tty/.
  To fix this, here what I have done.

  cd /etc/udev/rules.d

  sudo touch JCLL.rules

  The file contains :
  KERNEL=="ttyUSB1", MODE="0666"

  You then need to reboot your system.

## Developping software :
In the firmware folder, you will find a simple skeleton that helps understand how to developp software in C on picorv32.
***
Contact me :
<jean-christophe.le_lann@ensta-bretagne.fr>
